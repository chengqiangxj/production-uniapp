/* npm install uni-request --save */
import uniRequest from "uni-request";
/* url前缀 */
let base = 'http://localhost:9101';
/* 是否需要登录 */
let needLogin = true;
/* 请求拦截器 */
uniRequest.interceptors.request.use(request=>{
	console.log(`请求【${request.url}】${request.needLogin?'需要':'不需要'}登录`);
	if(request.needLogin){
	  if(uni.getStorageSync('userInfo')){
	      request.headers.Authorization = 'Bearer '+ uni.getStorageSync('userInfo').access_token;
	  }else{
	    uni.showToast({
	    	icon:'none',
			title:'请重新登录'
	    })
	    uni.navigateTo({
	    	url:'/pages/login/login.vue'
	    })
	  }
	}
	return request;
},err=>{
	return Promise.reject(err);
});
/* 响应拦截器 */
uniRequest.interceptors.response.use(success=> {
   if (success.status && success.status == 200 && success.data.status == 500){
	   uni.showToast({
	   	icon:'none',
	   	title:success.data.msg
	   })
       return;
   }
   if (success.status && success.status == 200 && success.data.code == 401){
	   uni.showToast({
	   	icon:'error',
	   	title:'请重新登录'
	   })
	   uni.navigateTo({
	   	url:'/pages/login/login.vue'
	   })
       return;
   }
   if (success.status && success.status == 200 && success.data.code == 403){
	   uni.showToast({
	   	icon:'error',
	   	title:'权限不足,请联系管理员!'
	   })
       return;
   }
   if (success.data.msg){
	   uni.showToast({
	   	icon:'success',
	   	title:success.data.msg
	   })
   }
   // 下载文件资源 type:文件类型
   if(success.data.type){
       return success;
   }
   return success.data;
}, err=> {
   if (error.response.status == 504){
	   uni.showToast({
	   	icon:'error',
	   	title:'技术人员正在更新，请稍后...'
	   })
   }else if (error.response.status == 403){
       uni.showToast({
       	icon:'error',
       	title:'权限不足,请联系管理员!'
       })
   }else if (error.response.status == 401){
       uni.showToast({
       	icon:'error',
       	title:'请重新登录'
       })
       uni.navigateTo({
       	url:'/pages/login/login.vue'
       })
   }else if (error.response.status == 500 && error.response.data.code == 504){
	   uni.showToast({
	   	icon:'error',
	   	title:error.response.data.message
	   })
   }else {
       if (error.response.data.msg){
		   uni.showToast({
		   	icon:'error',
		   	title:error.response.data.msg
		   })
       }else {
		   uni.showToast({
		   	icon:'error',
		   	title:'未知错误！'
		   })
       }
   }
   return;
});

/*封装请求方式,这里封装两个post请求,一个专门用来登录使用key-value的方式传参,
因为Spring Security登录默认不支持Json参数*/
export const postKeyValueRequest=(url,params)=>{
		return uniRequest({
			needLogin: false,
			method:'POST',
			url:`${base}${url}`,
			data:params,
			headers:{
			    'Content-Type':'application/x-www-form-urlencoded'
			}
		})
}
// Post请求
export const postRequest=(url,params)=>{
		return uniRequest({
			needLogin: true,
			method: 'POST',
			url:`${base}${url}`,
			data: params,
			headers:{
				'Content-Type':'application/json'
			}
		})
}
// Get请求
export const getRequest=(url,params)=>{
    	return uniRequest({
			needLogin: true,
			method:'GET',
			url:`${base}${url}`,
			data:params
		})
}
// Put请求
export const putRequest=(url,params)=>{
		return uniRequest({
			needLogin: true,
			method:'PUT',
			url:`${base}${url}`,
			data:params,
			headers:{
				'Content-Type':'application/json'
			}
	})
}
// Delete请求
export const deleteRequest=(url,params)=>{
    	return uniRequest({
			needLogin: true,
			method:'DELETE',
			url:`${base}${url}`,
			data:params
		})
}