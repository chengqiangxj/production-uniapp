import App from './App'

// #ifndef VUE3
import Vue from 'vue'
/* 挂载实例 */
import {postKeyValueRequest} from "./utils/request.js";
import {postRequest} from "./utils/request.js";
import {getRequest} from "./utils/request.js";
import {putRequest} from "./utils/request.js";
import {deleteRequest} from "./utils/request.js";
Vue.prototype.postKeyValueRequest = postKeyValueRequest;
Vue.prototype.postRequest = postRequest;
Vue.prototype.getRequest = getRequest;
Vue.prototype.putRequest = putRequest;
Vue.prototype.deleteRequest = deleteRequest;

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif